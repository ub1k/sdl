#include <stdio.h>
#include <stdlib.h>
#include "isoEngine.h"

void setupRect(SDL_Rect *rect, int x, int y, int w, int h)
{
    rect->x = x;
    rect->y = y;
    rect->w = w;
    rect->h = h;
}

void initIsoEngine(isoEngine *isoEngine, int tileSizeInPixels)
{
    if(isoEngine == NULL) {
        fprintf(stderr, "ERROR: initIsoEngine(): isoEngine is NULL.\n");
        return;
    }

    if(tileSizeInPixels <= 0) {
        TILESIZE = 32;
    } else {
        TILESIZE = tileSizeInPixels;
    }

    isoEngine->mapWidth = 0;
    isoEngine->mapHeight = 0;
    isoEngine->scrollX = 0;
    isoEngine->scrollY = 0;
}

void isoSetMapSize(isoEngine *isoEngine, int w, int h)
{
    if(isoEngine == NULL) {
        fprintf(stderr, "ERROR: isoSetMapSize(): isoEngine is NULL.\n");
        return;
    }
    isoEngine->mapWidth = w;
    isoEngine->mapHeight = h;
}

void convertWorldToScreen(Point *point)
{
    point->x = point->x - point->y;
    point->y = (point->x + point->y) / 2;
}

void convertScreenToWorld(Point *point)
{
    point->x = (2 * point->y + point->x) / 2;
    point->y = (2 * point->y - point->x) / 2;
}

void getTileCoordinates(Point *point, Point *point2DCoords)
{
    float tmp_x = (float)point->x / (float)TILESIZE;
    float tmp_y = (float)point->y / (float)TILESIZE;

    point2DCoords->x = (int)tmp_x;
    point2DCoords->y = (int)tmp_y;
}
