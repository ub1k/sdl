#ifndef ISOENGINE_H_
#define ISOENGINE_H_
#include <SDL2/SDL.h>

unsigned int TILESIZE;

typedef struct Point
{
    int x;
    int y;
} Point;

typedef struct isoEngine
{
    int scrollX;
    int scrollY;
    int mapWidth;
    int mapHeight;
} isoEngine;

void setupRect(SDL_Rect *rect, int x, int y, int w, int h);
void initIsoEngine(isoEngine *isoEngine, int tileSizeInPixels);
void isoSetMapSize(isoEngine *isoEngine, int w, int h);
void convertWorldToScreen(Point *point);
void convertScreenToWorld(Point *point);
void getTileCoordinates(Point *point, Point *point2DCoords);

#endif
