CC=gcc
CFLAGS=-g3 -Wall -Wextra -Wpedantic
LDFLAGS=-lm -lSDL2 -lSDL2_image


all:
	$(CC) $(CFLAGS) $(LDFLAGS) main.c renderer.c initclose.c texture.c isometry/isoEngine.c -o main

clean:
	rm -rf *.o main
