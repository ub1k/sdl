#ifndef __RENDERER_H
#define __RENDERER_H

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

void initRenderer(char *windowTitle);
SDL_Renderer *getRenderer();
extern SDL_Window *window;
#define getWindow() window
void closeRenderer();

#endif
