#ifndef UNIT_H_
#define UNIT_H

#include <SDL2/SDL.h>
#include <stdbool.h>
#include "../isometry/isoEngine.h"
#include "../texture.h"

struct Unit
{
    Point position;
    Point targetPosition;
    SDL_Rect *unitRects;
    Texture *unitTexture;
    bool isSelected;
    int velocity;
};

struct Unit unit_initialize();

#endif
