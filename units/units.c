#include "units.h"

struct Unit
unit_initialize(void)
{
    static struct Unit unit;

    unit.position.x = 0;
    unit.position.x = 0;
    unit.targetPosition.x = -1;
    unit.targetPosition.y = -1;
    unit.unitRects = NULL;
    unit.unitTexture = NULL;
    unit.isSelected = false;
    unit.velocity = 3;

    return unit;
}
