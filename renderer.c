#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include "renderer.h"

SDL_Window *window = NULL;
static SDL_Renderer *renderer = NULL;

void initRenderer(char *windowTitle) {
    if (window == NULL) {
        window = SDL_CreateWindow(windowTitle,
                                  SDL_WINDOWPOS_CENTERED,
                                  SDL_WINDOWPOS_CENTERED,
                                  WINDOW_WIDTH,
                                  WINDOW_HEIGHT,
                                  SDL_WINDOW_RESIZABLE);
        if( window == NULL) {
            fprintf(stderr, "SDL_CreateWindow failed: %s\n", SDL_GetError());
            exit(1);
        }
    }
}

SDL_Renderer *getRenderer(void)
{
    if (renderer == NULL) {
        uint32_t flags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE | SDL_RENDERER_PRESENTVSYNC;
        renderer = SDL_CreateRenderer(window, -1, flags);
        if(renderer == NULL) {
            fprintf(stderr, "SDL_CreateRenderer failed: %s\n", SDL_GetError());
            exit(EXIT_FAILURE);
        }

    }

    return renderer;
}

void closeRenderer()
{
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
}
