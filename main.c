#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <stdbool.h>
#include "renderer.h"
#include "initclose.h"
#include "texture.h"
#include "isometry/isoEngine.h"
#include "units/units.h"

#define MAP_WIDTH 10
#define MAP_HEIGHT 10
#define MAP_TILES 4
#define TILESIZE 64

#define UNIT_TILES 8
#define UNIT_NUM 2

static const int SCR_WIDTH  = 800;
static const int SCR_HEIGHT = 600;

/* static int worldMap[MAP_WIDTH][MAP_HEIGHT] = { */
/*     {0, 3, 1, 0, 0, 0, 0, 1, 1, 1}, */
/*     {0, 3, 3, 1, 0, 0, 0, 0, 1, 0}, */
/*     {0, 1, 3, 3, 1, 0, 0, 0, 0, 0}, */
/*     {0, 0, 1, 3, 1, 0, 0, 0, 0, 0}, */
/*     {0, 0, 1, 3, 1, 0, 0, 0, 0, 0}, */
/*     {0, 1, 1, 3, 1, 0, 0, 0, 0, 2}, */
/*     {1, 3, 3, 3, 1, 0, 0, 0, 2, 2}, */
/*     {1, 3, 1, 1, 1, 0, 0, 2, 2, 2}, */
/*     {3, 3, 3, 1, 1, 0, 2, 2, 2, 2}, */
/*     {3, 3, 3, 3, 1, 2, 2, 2, 2, 0} */
/* }; */

struct TILE {
    Texture *texture;
    SDL_Rect *rects;
    struct Unit *unit;
    Point coords;
    int index;
} tiles[MAP_HEIGHT][MAP_WIDTH];

typedef struct Game
{
    struct Unit *unit;

    Point mapScroll2Dpos;
    Point mousePoint;
	bool isLeftMouseButtonPressed;
	bool select;
	Point startSelection;

    Point tilePosition;
    SDL_Event event;
    int loopDone;
    int mapScrollSpeed;
}Game;

static Game game;

/* map */
static Texture tilesTexture;
static SDL_Rect tilesRects[MAP_TILES];

/* unit */
Texture unitTexture;
SDL_Rect unitRects[UNIT_TILES];

static void initMapTileClip(void)
{
    int x = 0;
    int y = 0;
    int i;

    textureInit(&tilesTexture, 0, 0, 0, NULL, NULL, SDL_FLIP_NONE);

    for(i = 0; i < MAP_TILES; ++i) {
        setupRect(&tilesRects[i], x, y, 64, 64);
        x += 64;
    }
}

static void initCharClip(void)
{
    int x = 0;
    int y = 0;
    int i;

    textureInit(&unitTexture, 0, 0, 0, NULL, NULL, SDL_FLIP_NONE);
    for(i = 0; i < UNIT_TILES; ++i) {
        setupRect(&unitRects[i], x, y, 64, 64);
	        x += 64;
    }
}

static void initialize(void)
{
    game.loopDone = 0;

    initMapTileClip();
    initCharClip();

    game.mapScroll2Dpos.x = 0;
    game.mapScroll2Dpos.y = 0;
    game.mapScrollSpeed = 12;

	game.isLeftMouseButtonPressed = false;
	game.startSelection.x = -1;
	game.startSelection.y = -1;
	game.select = false;

    game.mousePoint.x = 0;
    game.mousePoint.y = 0;

    if(loadTexture(&tilesTexture, "sprites/maptiles.png") == 0) {
        fprintf(stderr,
                "ERROR: could not load texture 'sprites/maptiles.png'.\n");
        exit(1);
    }

    if(loadTexture(&unitTexture, "sprites/char.png") == 0) {
        fprintf(stderr, "ERROR: could not load texture 'sprites/char.png'.\n");
        exit(1);
    }

    /* tiles initialization */
    for(int i = 0; i < MAP_HEIGHT; ++i) {
        for(int j = 0; j < MAP_WIDTH; ++j) {
            tiles[i][j].texture = &tilesTexture;
            tiles[i][j].rects = &tilesRects[0];
            tiles[i][j].unit = NULL;
            tiles[i][j].coords.x = i * TILESIZE;
            tiles[i][j].coords.y = j * TILESIZE;
            tiles[i][j].index = (tiles[i][j].coords.y * MAP_WIDTH + tiles[i][j].coords.x) / TILESIZE;
            printf("tile # %d initialized.\n", tiles[i][j].index);
        }
    }

    /* units initialization */
    game.unit = malloc(UNIT_NUM * sizeof(struct Unit));

    if(game.unit == NULL) {
        fprintf(stderr, "Unable to allocate memory for game.unit!\n");
        exit(1);
    }

    for(int i = 0; i < UNIT_NUM; i++) {
        game.unit[i] = unit_initialize();
        game.unit[i].unitTexture = &unitTexture;
        game.unit[i].unitRects = unitRects;
    }

    game.unit[1].position.x = 64;
    game.unit[1].position.y = 0;

    tiles[0][0].unit = &game.unit[0];
    tiles[1][0].unit = &game.unit[1];

    printf("unit 0 in game: %p\n", &game.unit[0]);
    printf("unit 0 in tile: %p\n", tiles[0][0].unit);

    printf("unit 1 in game: %p\n", &game.unit[1]);
    printf("unit 1 in tile: %p\n", tiles[1][0].unit);
}

static void drawMap(void)
{
    int i, j;
    Point point;

    /* int tile = 4; */

    for(i = 0; i < MAP_WIDTH; ++i) {
        for(j = 0; j < MAP_HEIGHT; ++j) {
            point.x = j * TILESIZE + game.mapScroll2Dpos.x;
            point.y = i * TILESIZE + game.mapScroll2Dpos.y;

            /* tile = worldMap[i][j]; /\* get current tile type *\/ */
            /* textureRenderXYClip(&tilesTexture, point.x, point.y, */
            /*                     &tilesRects[tile]); */

            textureRenderXYClip(tiles[i][j].texture, point.x, point.y,
                                tiles[i][j].rects);
        }
    }
}

static void get_tile_index(Point mouse)
{
    int tmpx = (mouse.x - game.mapScroll2Dpos.x) / TILESIZE;
    int tmpy = (mouse.y - game.mapScroll2Dpos.y) / TILESIZE;
    game.tilePosition.x = tmpx;
    game.tilePosition.y = tmpy;

    printf("tilePosition.x = %d, tilePosition.y = %d\n", game.tilePosition.x, game.tilePosition.y);
}

static void scrollMap(void)
{

    if(game.mousePoint.x < 2) {
        game.mapScroll2Dpos.x += game.mapScrollSpeed;
    }
    if(game.mousePoint.x > WINDOW_WIDTH - 2) {
        game.mapScroll2Dpos.x -= game.mapScrollSpeed;
    }
    if(game.mousePoint.y < 2) {
        game.mapScroll2Dpos.y += game.mapScrollSpeed;
    }
    if(game.mousePoint.y > WINDOW_HEIGHT -2) {
        game.mapScroll2Dpos.y -= game.mapScrollSpeed;
    }
}

static void drawCharacter(Texture *texture,
                          const Point *map,
                          const Point *char_,
                          SDL_Rect *rect)
{
    textureRenderXYClip(texture,
                        char_->x + map->x,
                        char_->y + map->y,
                        rect);
}

static void moveCharacter(Point *cp, Point *tp, int velocity)
{
	/* cp - charPoint,
     * tp - targetPosition
     */

	if(tp->x == -1 || tp->y == -1) {
		return;
	}

	/* tp adjustment to the map edges */
	if(tp->x > MAP_WIDTH * TILESIZE- TILESIZE) {
		tp->x = MAP_WIDTH * TILESIZE - TILESIZE;
	}

	if(tp->y > MAP_HEIGHT * TILESIZE - TILESIZE) {
		tp->y = MAP_WIDTH * TILESIZE - TILESIZE;
	}

	if(cp->x < tp->x) {
		if(cp->x + velocity >= tp->x) {
			cp->x = tp->x;
		} else {
			cp->x += velocity;
		}
	}
	if(cp->y < tp->y) {
		if(cp->y + velocity >= tp->y) {
			cp->y = tp->y;
		} else {
			cp->y += velocity;
		}
	}

	if(cp->x > tp->x) {
		if(cp->x - velocity <= tp->x) {
			cp->x = tp->x;
		} else {
			cp->x -= velocity;
		}
	}
	if(cp->y > tp->y) {
		if(cp->y - velocity <= tp->y) {
			cp->y = tp->y;
		} else {
			cp->y -= velocity;
		}
	}

	if(cp->x == tp->x && cp->y == tp->y) {
		tp->x = -1;
		tp->y = -1;
	}
}

static void drawSelectionRectangle(Point *start, Point *end)
{
	/* printf("start: %d, %d\n", start->x, start->y); */
	/* printf("end: %d, %d\n", end->x, end->y); */
	SDL_Rect border = { start->x, start->y, end->x - start->x, end->y - start->y};
	SDL_SetRenderDrawBlendMode(getRenderer(), SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(getRenderer(), 0x4e, 0xff, 0x00, 0xff );
	SDL_RenderDrawRect(getRenderer(), &border );

	SDL_Rect inner = { start->x + 2, start->y + 2, (end->x - start->x) - 4, (end->y - start->y) - 4};
	SDL_SetRenderDrawColor(getRenderer(), 0x4e, 0xff, 0x00, 0x40 );
	SDL_RenderFillRect(getRenderer(), &inner );
}

static void draw(void)
{
    SDL_SetRenderDrawColor(getRenderer(), 0x3f, 0x3f, 0x3f, 0x3f);
    SDL_RenderClear(getRenderer());

    drawMap();
    for(int i = 0; i < UNIT_NUM; i++) {
        drawCharacter(game.unit[i].unitTexture,
                      &game.mapScroll2Dpos,
                      &game.unit[i].position,
                      &game.unit[i].unitRects[0]);
    }

	if(game.select) {
		drawSelectionRectangle(&game.startSelection, &game.mousePoint);
	}

    SDL_RenderPresent(getRenderer());

    /* Do not be a CPU HOG! */
    SDL_Delay(10);
}

static void update(void)
{
    /* get mouse coordinates */
    SDL_GetMouseState(&game.mousePoint.x, &game.mousePoint.y);
    scrollMap();

    if(game.unit->isSelected) {
        moveCharacter(&game.unit[0].position,
                      &game.unit[0].targetPosition,
                      game.unit[0].velocity);
    }
}

static int
onLeftMouseButtonDown(bool *isSelected,
                      const Point *mouse,
                      const Point *map,
                      const Point *char_)
{
    if (!*isSelected) {
        if (mouse->x - map->x >= char_->x &&
            mouse->x - map->x <= char_->x + TILESIZE &&
            mouse->y - map->y >= char_->y &&
            mouse->y - map->y <= char_->y + TILESIZE)
        {
            *isSelected = true;
            printf("The unit is selected!\n");
            return 1;
        }
    } else {
        if (mouse->x - map->x < char_->x ||
            mouse->y - map->y < char_->y ||
            mouse->x - map->x > char_->x + TILESIZE ||
            mouse->y - map->y > char_->y + TILESIZE)
        {
            *isSelected = false;
            printf("The unist is unselected!\n");
            return 1;
        }
    }
    return 0;
}

static void
onRightMouseButtonDown(bool *isSelected,
                       const Point *mouse,
                       const Point *map,
                       Point *target)
{
    if (*isSelected) {
        if (mouse->x - map->x >= 0 &&
            mouse->y - map->y >= 0 &&
            mouse->x - map->x <= MAP_WIDTH * TILESIZE &&
            mouse->y - map->y <= MAP_HEIGHT * TILESIZE)
        {
            target->x = mouse->x - map->x;
            target->y = mouse->y - map->y;
            printf("Move character here: %d x %d\n", target->x, target->y);
        }
    } else {
        *isSelected = false;
        printf("The unit is unselected!\n");
    }
}

static void updateInput(void)
{
    /* const Uint8 *keystate = SDL_GetKeyboardState(NULL); */

    while(SDL_PollEvent(&game.event) != 0) {
        switch(game.event.type) {

        case SDL_QUIT:
            game.loopDone = 1;
            break;

        case SDL_KEYUP:
            switch(game.event.key.keysym.sym) {
            case SDLK_ESCAPE:
                game.loopDone = 1;
                break;

            default: break;
            }
            break;

        case SDL_MOUSEBUTTONDOWN:
            get_tile_index(game.mousePoint);
            printf("unit: %p\n", tiles[game.tilePosition.x][game.tilePosition.y].unit);
            if (game.event.button.button == SDL_BUTTON_LEFT &&
                /* onLeftMouseButtonDown(&game.unit[0].isSelected, */
                /*                       &game.mousePoint, */
                /*                       &game.mapScroll2Dpos, */
                /*                       &game.unit[0].position)) { */
                onLeftMouseButtonDown(&tiles[game.tilePosition.x][game.tilePosition.y].unit->isSelected,
                                      &game.mousePoint,
                                      &game.mapScroll2Dpos,
                                      &tiles[game.tilePosition.x][game.tilePosition.y].unit->position)) {

				break;
            }
            if (game.event.button.button == SDL_BUTTON_RIGHT) {
                onRightMouseButtonDown(&game.unit[0].isSelected,
                                       &game.mousePoint,
                                       &game.mapScroll2Dpos,
                                       &game.unit[0].targetPosition);
                break;
            }

			/* draw selection rectangle */
			if (game.event.button.button == SDL_BUTTON_LEFT &&
                !onLeftMouseButtonDown(&game.unit[0].isSelected,
                                      &game.mousePoint,
                                      &game.mapScroll2Dpos,
                                      &game.unit[0].position)) {
				game.isLeftMouseButtonPressed = true;
				SDL_GetMouseState(&game.startSelection.x,
                                  &game.startSelection.y);
				/* game.startSelection.x -= game.mapScroll2Dpos.x; */
				/* game.startSelection.y -= game.mapScroll2Dpos.y; */
				break;
			}

            break;

		case SDL_MOUSEMOTION:
			if(game.isLeftMouseButtonPressed) {
				game.select = true;
			}
			break;

		case SDL_MOUSEBUTTONUP:
			if(game.isLeftMouseButtonPressed) {
				game.isLeftMouseButtonPressed = false;
				game.select = false;
				game.startSelection.x = -1;
				game.startSelection.y = -1;
			}
			break;

        default:
            break;
        }
    }

	/* if(keystate[SDL_SCANCODE_S]) { */
    /*     if((game.character->charPoint.y + TILESIZE) >= (MAP_HEIGHT * TILESIZE)) { */
    /*         game.character->charPoint.y = (MAP_HEIGHT * TILESIZE) - TILESIZE; */
    /*     } else { */
    /*         game.character->charPoint.y += 5; */
    /*     } */
    /* } else if(keystate[SDL_SCANCODE_W]) { */
    /*     if (game.character->charPoint.y <= 0) { */
    /*         game.character->charPoint.y = 0; */
    /*     } else { */
    /*         game.character->charPoint.y -= 5; */
    /*     } */
    /* } else if(keystate[SDL_SCANCODE_A]) { */
    /*     if(game.character->charPoint.x <= 0) { */
    /*         game.character->charPoint.x = 0; */
    /*     } else { */
    /*         game.character->charPoint.x -= 5; */
    /*     } */
    /* } else if(keystate[SDL_SCANCODE_D]) { */
    /*     if(game.character->charPoint.x + TILESIZE >= MAP_WIDTH * TILESIZE) { */
    /*         game.character->charPoint.x = (MAP_WIDTH * TILESIZE) - TILESIZE; */
    /*     } else { */
    /*         game.character->charPoint.x += 5; */
    /*     } */
    /* } */
}

int main()
{
    initSDL("OpenStrike");
    initialize();

    /* hide system cursor */
    /* SDL_ShowCursor(0); */

	/* SDL_SetWindowGrab(getWindow(), SDL_TRUE); */
	/* SDL_WarpMouseInWindow(getWindow(), WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2); */

    while(!game.loopDone) {
        update();
        updateInput();
        draw();
    }

    closeDownSDL();

    return 0;
}
